package org.example;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;

import org.beanio.BeanReader;
import org.beanio.StreamFactory;
import org.beanio.builder.StreamBuilder;
import org.junit.Test;

public class TransactionTest {

    @Test
    public void shouldBuildTransactionCorrectly() {

        // given: all data (test fixture) preparation
        final StreamFactory factory = StreamFactory.newInstance();
        final StreamBuilder builder = new StreamBuilder("transactionFile").format("fixedlength").addRecord(Transaction.class);
        factory.define(builder);

        // when : method to be checked invocation
        BeanReader reader = null;

        try {
            reader = factory.createReader("transactionFile", new File("src/test/resources/Input1st3Lines.txt"));
            Transaction transaction;

            while ((transaction = (Transaction) reader.read()) != null) {
                // then : checks and assertions
                assertThat(transaction.getRecordCode(), is("315"));
                assertThat(transaction.getClientType(), is("CL"));
                assertThat(transaction.getClientNumber(), is("4321"));
                assertThat(transaction.getAccountNumber(), is("0002"));
                assertThat(transaction.getSubAccountNumber(), is("0001"));
                assertThat(transaction.getOppositePartyCode(), is("SGXDC"));
                assertThat(transaction.getProductGroupCode(), is("FU"));
                assertThat(transaction.getExchangeCode(), is("SGX"));
                assertThat(transaction.getSymbol(), is("NK"));
                assertThat(transaction.getExpirationDate(), is("20100910"));
                assertThat(transaction.getQuantityLongSign(), is('-'));
                assertThat(transaction.getQuantityLong(), is(1L));
                assertThat(transaction.getQuantityShortSign(), is(' '));
                assertThat(transaction.getQuantityShort(), is(0L));
                assertThat(transaction.getTransactionDate(), is("20100820"));
            }
        } finally {
            if (null != reader) {
                reader.close();
                reader = null;
            }
        }
    }
}
