package org.example;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;

public class FileProcessorTest {

    @Test
    public void shouldProcessInputCorrectly() throws IOException {

        // given: all data (test fixture) preparation
        final FileProcessor fileProcessor = new FileProcessor();

        // when : method to be checked invocation
        fileProcessor.summarize("20100819", new File("src/test/resources/Input.txt"));

        // then : checks and assertions
        final List<String> lines = new ArrayList<>();

        try (final Scanner scanner = new Scanner(new File("Output.csv"))) {
            scanner.useDelimiter("\\R");

            while (scanner.hasNext()) {
                lines.add(scanner.next());
            }
        }
        assertThat(lines, hasSize(4));
        assertThat(lines.toArray()[0], is("Client_Information,Product_Information,Total_Transaction_Amount"));
        assertThat(lines.toArray()[1], is("CL_4321_0003_0001,CME_FU_N1_20100910,-79"));
        assertThat(lines.toArray()[2], is("CL_1234_0003_0001,CME_FU_N1_20100910,285"));
        assertThat(lines.toArray()[3], is("CL_1234_0003_0001,CME_FU_NK._20100910,-215"));
    }
}
