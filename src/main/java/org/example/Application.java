package org.example;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Application {
    public static void main(final String[] args) {

        if (args.length < 2) {
            showUsage();
        }

        try {
            LocalDate.parse(args[0], DateTimeFormatter.BASIC_ISO_DATE);
        } catch (final Exception e) {
            log.error("parse transactionDate provided as argument ", e);
            showUsage();
        }

        final File inputFileName = new File(args[1]);

        if (!inputFileName.exists() || !inputFileName.canRead()) {
            log.error("unable to read file name/path provided as argument ", args[1]);
            showUsage();
        }

        log.info("Processing {} ...", inputFileName.getPath());
        final FileProcessor fileProcessor = new FileProcessor();

        try {
            fileProcessor.summarize(args[0], inputFileName);
        } catch (final Exception e) {
            log.error("error summarizing input ", e);
            System.exit(1);
        }
        log.info("Done processing, see Output.csv in the current folder");
    }

    private static void showUsage() {
        System.err.println(System.lineSeparator() + "Usage: java -jar summary-report.jar transactionDate inputFileName");
        System.err.println("       transactionDate - the date the transactions needs to be summarized (CCYYMMDD)");
        System.err.println("       inputFileName - the full path of the input file");
        System.err.println(System.lineSeparator() + "Note: the output file Output.csv will be created in the current folder");
        System.exit(1);
    }
}
