package org.example;

import org.beanio.annotation.Field;
import org.beanio.annotation.Record;

import lombok.Data;

@Data
@Record
public class Transaction {

    private static final char DELIMITER = '_';
    private static final char MINUS_SIGN = '-';

    @Field(length = 3)
    private String recordCode;

    @Field(length = 4)
    private String clientType;

    @Field(length = 4)
    private String clientNumber;

    @Field(length = 4)
    private String accountNumber;

    @Field(length = 4)
    private String subAccountNumber;

    @Field(length = 6)
    private String oppositePartyCode;

    @Field(length = 2)
    private String productGroupCode;

    @Field(length = 4)
    private String exchangeCode;

    @Field(length = 6)
    private String symbol;

    @Field(length = 8)
    private String expirationDate;

    @Field(length = 6)
    private String unusedOne;

    @Field(length = 1)
    private char quantityLongSign;

    @Field(length = 10)
    private long quantityLong;

    @Field(length = 1)
    private char quantityShortSign;

    @Field(length = 10)
    private long quantityShort;

    @Field(length = 48)
    private String unusedTwo;

    @Field(length = 8)
    private String transactionDate;

    String getClientInformation() {
        return clientType + DELIMITER + clientNumber + DELIMITER + accountNumber + DELIMITER + subAccountNumber;
    }

    String getProductInformation() {
        return exchangeCode + DELIMITER + productGroupCode + DELIMITER + symbol + DELIMITER + expirationDate;
    }

    long getTotalTransactionAmount() {
        final long signedQuantityLong = quantityLong * (MINUS_SIGN == quantityLongSign ? -1 : 1);
        final long signedQuantityShort = quantityShort * (MINUS_SIGN == quantityShortSign ? -1 : 1);
        return signedQuantityLong - signedQuantityShort;
    }
}
