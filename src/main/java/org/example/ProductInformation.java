package org.example;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductInformation {

    private final String key;
    private long totalTransactionAmount;

    public void accumulateToTotalTransactionAmount(final long aTotalTransactionAmount) {
        totalTransactionAmount += aTotalTransactionAmount;
    }
}
