package org.example;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientInformation {

    private final String key;
    private final Map<String, ProductInformation> products;

}
