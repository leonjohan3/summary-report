package org.example;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.beanio.BeanReader;
import org.beanio.StreamFactory;
import org.beanio.builder.StreamBuilder;

public class FileProcessor {

    private static final String BUILDER_NAME = "transactionFile";

    public void summarize(final String transactionDate, final File inputFile) throws IOException {

        final StreamFactory factory = StreamFactory.newInstance();
        final StreamBuilder builder = new StreamBuilder(BUILDER_NAME).format("fixedlength").addRecord(Transaction.class);
        factory.define(builder);
        BeanReader reader = null;
        final Map<String, ClientInformation> clients = new HashMap<>();

        try {

            reader = factory.createReader(BUILDER_NAME, inputFile);
            Transaction transaction;

            while ((transaction = (Transaction) reader.read()) != null) {

                if (transactionDate.equals(transaction.getTransactionDate())) {

                    if (null == clients.get(transaction.getClientInformation())) {
                        clients.put(transaction.getClientInformation(), new ClientInformation(transaction.getClientInformation(), new HashMap<>()));
                    }
                    final ClientInformation clientInformation = clients.get(transaction.getClientInformation());

                    if (null == clientInformation.getProducts().get(transaction.getProductInformation())) {
                        final ProductInformation productInformation = new ProductInformation(transaction.getProductInformation(),
                                        transaction.getTotalTransactionAmount());
                        clientInformation.getProducts().put(transaction.getProductInformation(), productInformation);
                    } else {
                        final ProductInformation productInformation = clientInformation.getProducts().get(transaction.getProductInformation());
                        productInformation.accumulateToTotalTransactionAmount(transaction.getTotalTransactionAmount());
                    }
                }
            }

        } finally {
            if (null != reader) {
                reader.close();
                reader = null;
            }
        }

        try (PrintWriter writer = new PrintWriter("Output.csv")) {

            writer.println("Client_Information,Product_Information,Total_Transaction_Amount");

            for (final ClientInformation clientInformation : clients.values()) {
                for (final ProductInformation productInformation : clientInformation.getProducts().values()) {
                    writer.printf("%s,%s,%d%n", clientInformation.getKey(), productInformation.getKey(), productInformation.getTotalTransactionAmount());
                }
            }
        }
    }
}
